using InMemeoryTest.Models;
using Microsoft.EntityFrameworkCore;

namespace InMemeoryTest.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base() {}
        
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        { }
        
        public DbSet<Client> Clients { get; set; }
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0");
            }
        }
    }
}