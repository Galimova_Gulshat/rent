using System.ComponentModel.DataAnnotations;

namespace InMemeoryTest.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Не указано имя")]
        public string FirstName { get; set; }
        
        [Required(ErrorMessage = "Не указана фамилия")]
        public string LastName { get; set; }
        
        [Required(ErrorMessage = "Не указан Email")]
        public string Email { get; set; }
 
        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
 
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароль введен неверно")]
        public string ConfirmPassword { get; set; }
        
        public int Series { get; set; }
        
        public int Number { get; set; }
        
        public string IssuePlace { get; set; }
    }
}