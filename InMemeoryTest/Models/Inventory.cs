namespace InMemeoryTest.Models
{
    public class Inventory
    {
        public int Id { get; set; }
        
        public int Count { get; set; }
        
        public InventoryType Type { get; set; }
        
        public int MinTime { get; set; }
    }

    public enum InventoryType
    {
        Bike = 0,
        Scooter = 1,
        HydroScooter = 2
    }
}