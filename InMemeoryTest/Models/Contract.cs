using System;

namespace InMemeoryTest.Models
{
    public class Contract
    {
        public int Id { get; set; }
        
        public int ClientId { get; set; }
        
        public int InventoryId { get; set; }
        
        public InventoryType InventoryType { get; set; }
        
        public int Count { get; set; }
        
        public bool IsReserved { get; set; }
        
        public DateTime RentBeginning { get; set; } 
        
        public DateTime RentEnding { get; set; }
        
        public int Cost { get; set; }
    }
}