using System;

namespace InMemeoryTest.Models
{
    public class Client
    {
        public int Id { get; set; }
        
        public string Email { get; set; }
        
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        public int Series { get; set; }
        
        public int Number { get; set; }
        
        public string IssuePlace { get; set; }
        
        public DateTime IssueDateTime { get; set; }
    }
}