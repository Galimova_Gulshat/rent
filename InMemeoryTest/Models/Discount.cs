namespace InMemeoryTest.Models
{
    public class Discount
    {
        public int Id { get; set; }
        
        public string Title { get; set; }
        
        public int Percent { get; set; }
    }
}