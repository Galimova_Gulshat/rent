using System;
using System.Linq;
using InMemeoryTest.Data;
using InMemeoryTest.Models;
using Microsoft.AspNetCore.Mvc;

namespace InMemeoryTest.Controllers
{
    public class RentController : Controller
    {
        private DatabaseContext _context;

        public RentController(DatabaseContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View(_context.Inventories);
        }
        
        [HttpPost]
        public IActionResult Booking(int id, int count, string isReserved)
        {
            var reserved = isReserved == null ? false : true;
            var userId = _context.Clients.First(c => c.Email == User.Identity.Name).Id;
            _context.Contracts.Add(new Contract
                {ClientId = userId, Count = count, InventoryId = id, RentBeginning = DateTime.Today, IsReserved = reserved});
            _context.SaveChanges();
            return View("Index");
        }

        

        public IActionResult UserRent()
        {
            var userId = _context.Clients.First(c => c.Email == User.Identity.Name).Id;
            return View(_context.Contracts.Where(c => c.ClientId == userId));
        }

        public IActionResult FinishRent(int id)
        {
            var contract = _context.Contracts.First(c => c.Id == id);
            contract.RentEnding = DateTime.Now;
            var cost = 0;
            var dur = (contract.RentEnding - contract.RentBeginning);
            if (contract.InventoryType == InventoryType.HydroScooter)
            {
                if (dur.Minutes <= 60)
                    cost = dur.Minutes / 30 * 500;
                else
                {
                    cost = 500 * 2 + (dur.Minutes - 60) / 60 * 750;
                }
            }
            else
            {
                if (dur.Hours <= 2)
                    cost = dur.Hours * 200;
                else if(dur.Hours < 12)
                {
                    cost = 400 + (dur.Hours - 2) * 100;
                }
                else if(dur.Hours == 12)
                {
                    cost = 600;
                }
                else
                {
                    cost = 1000;
                }
            }

            contract.Cost = cost * contract.Count;
            return RedirectToAction("UserRent");
        }
    }
}