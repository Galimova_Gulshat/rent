﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using InMemeoryTest.Data;
using Microsoft.AspNetCore.Mvc;
using InMemeoryTest.Models;

namespace InMemeoryTest.Controllers
{
    public class HomeController : Controller
    {
        private DatabaseContext _context;

        public HomeController(DatabaseContext context)
        {
            _context = context;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AddUser()
        {
            _context.Inventories.Add(new Inventory {Count = 15, Type = InventoryType.Bike, MinTime = 60});
            _context.Inventories.Add(new Inventory {Count = 10, Type = InventoryType.Scooter, MinTime = 60});
            _context.Inventories.Add(new Inventory {Count = 15, Type = InventoryType.HydroScooter, MinTime = 30});
            //_context.Clients.Add(new Client {FirstName = "Arina", LastName = "Borisova"});
            _context.SaveChanges();
            return View("Index");
        }

        public IActionResult Database()
        {
            return View(_context.Clients);
        }
        
        [HttpPost]
        public IActionResult Contracts(DateTime start, DateTime end)
        {
            var contracts = _context.Contracts.Where(c => c.RentBeginning >= start && c.RentEnding <= end);
            return View(contracts);
        }
        
        [HttpGet]
        public IActionResult EditClient()
        {
            return View(_context.Clients.First(c => c.Email == User.Identity.Name));
        }
        
        [HttpPost]
        public IActionResult EditClient(Client client)
        {
            var oldClient = _context.Clients.FirstOrDefault(c => c.Email == User.Identity.Name);
            oldClient.Number = client.Number;
            oldClient.Series = client.Series;
            oldClient.LastName = client.LastName;
            oldClient.FirstName = client.FirstName;
            oldClient.IssuePlace = client.IssuePlace;
            oldClient.IssueDateTime = client.IssueDateTime;
            _context.SaveChanges();
            return View("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}